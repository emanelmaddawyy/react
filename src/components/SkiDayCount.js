import React from 'react'
import '../stylesheet/ui.scss'
import Terrain from 'react-icons/lib/md/terrain'
import FaAdjust from 'react-icons/lib/fa/adjust'
    const percentToDecimal = (decimal) => {
        return ((decimal * 100) + '%')
    } 
 const calcGoalProgress = (total , goal) => {
        return percentToDecimal(total/goal)
 }
   
export const SkiDayCount = ({total , powder , backcountry , goal}) => 
        (
    <div className = "ski-day-count">
    <div className ="total-days">
    <span>{total}</span>
    <Terrain/>
    <span>days</span>
    </div>
    <div className ="powder-days">
    <span>{powder}</span>
    <FaAdjust/>
    <span> days</span>
    </div>
    <div className ="backcountry-days">
    <span>{backcountry}</span>
    <span>days</span>
    </div>
    <span>{calcGoalProgress(
        total,
        goal
    )}</span>
    </div>
)
