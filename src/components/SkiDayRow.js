import FaAdjust from 'react-icons/lib/fa/adjust'
import Calender from 'react-icons/lib/fa/calendar'


export const SkiDayRow = ({resort, date, powder,backcountry}) => (
    <tr>
      <td>
          {date.getMonth() +1} /{date.getDate()}/{date.getFullYear()}
      </td>
      <td> {resort} </td>
      <td> {(powder) ? <FaAdjust/> :null} </td>
      <td>{(backcountry) ? <Calender/> :null}</td>
    </tr>
)