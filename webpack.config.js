var webpack = require("webpack");
var path = require("path");

module.exports = {
    entry : path.resolve( __dirname +"/src/index.js"),
    output : {
        path: path.resolve(__dirname + "/dist/assets"),
        filename: "bundle.js",
        publicPath:"assets"
    },
    devServer: {
        inline :true,
        contentBase: './dist',
        port:3000
    },
    module: {
        rules :[
            {
                test:/\.js$/,
                exclude:/(node_modules)/,
                loader : "babel-loader",
                query : {
                    presets :["latest", "stage-0", "react"]
                }

            },
            {
            test:/\.json$/,
            exclude:/(node_modules)/
            }
            ,
            {
                test:/\.css$/,
                loader:'style-loader!css-loader!postcss-loader'
          
            },
            {
                test:/\.scss$/,
                loader:'style-loader!css-loader!postcss-loader!sass-loader'
            }
        ]
    }
}