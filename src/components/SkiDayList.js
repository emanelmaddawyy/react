//import FaAdjust from 'react-icons/lib/fa/adjust'
//import Calender from 'react-icons/lib/fa/calender'
import { SkiDayRow} from './SkiDayRow'
export const SkiDayList = ({days}) => (
<table>
    <thead>
     <tr>
      <th>Date</th>
      <th>Ressort</th> 
      <th>Powder</th>
      <th>Backcountry</th>
    </tr>
  </thead>
  <tbody>
      {days.map((day,i)=>
   <SkiDayRow key={i}
            resort={day.resort}
            date= {day.date}
            powder={day.powder}
            backcountry ={day.backcountry} />
    )}
  </tbody>
</table>
)
