import React from 'react'
import { render} from 'react-dom'
import { SkiDayCount } from './components/SkiDayCount'
import { SkiDayList} from './components/SkiDayList'

window.React = React
render(
    <div>
        <SkiDayCount total={50}
                powder={20}
                backcountry={10}
                goal={100} />

                <SkiDayList days ={
                    [
                        {
                            resort: "Squaw Vallley",
                            date: new Date("1/2/2016"),
                            powder :true,
                            backcountry:true
                        },
                        {
                            resort: "Kirkwood",
                            date: new Date("11/5/2016"),
                            powder :true,
                            backcountry:true
                        },
                        {
                            resort: "Mt.Tallac",
                            date: new Date("12/22/2016"),

                            powder :true,
                            backcountry:true
                        }
                    ]
                 }/>
               
    </div>,
    document.getElementById("react-body")
)
